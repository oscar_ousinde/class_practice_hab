/* #################
 * ## Ejercicio 2 ##
 * #################
 *
 * Crea un programa que compruebe si un string es palíndromo, es decir, que se lee igual
 * del derecho que del revés. Por ejemplo, "Arriba la birra" es un palíndromo.
 *
 * Nota: el string que utilizaremos como input no debe tener caracteres no presentes
 * en el alfabeto inglés ni símbolos.
 *
 */

'use strict';
/* const userWord = 'palindrome'.replaceAll(' ', '').toLowerCase();

const palindromeCheck = userWord.split('').reverse().join('');

if (userWord === palindromeCheck) {
    alert(`${userWord} es un palíndromo!`);
} else {
    alert(`${userWord} no es un palíndromo!`);
} */

const checkPalindrome = (userWord) => {
    userWord = userWord.toLowerCase().replaceAll(' ', '');
    const userWordModified = userWord.split('').reverse().join('');
    if (userWord === userWordModified) {
        alert(`${userWord} es un palíndromo!`);
    } else {
        alert(`${userWord} no es un palíndromo!`);
    }
};

checkPalindrome(prompt('Comprueba tu palíndromo'));
