/**
 * #################
 * ## Ejercicio 6 ##
 * #################
 *
 * A partir del array de estudiantes dados
 *
 *  - Crea un nuevo array donde solo esten incluídos los estudiantes que están cursando JavaScript ("js").
 *
 *  - Crea un nuevo array donde solo esten incluídos los estudiantes menores de 30 años.
 *
 *  - Agrega a todos los estudiantes del array original la propiedad "isAdult". Deberá ser true o false
 *    en función de la edad.
 *
 */

'use strict';

const students = [
    {
        name: 'Carla',
        course: 'js',
        age: 37,
    },
    {
        name: 'Jose',
        course: 'java',
        age: 17,
    },
    {
        name: 'Pablo',
        course: 'python',
        age: 47,
    },
    {
        name: 'Lucía',
        course: 'js',
        age: 51,
    },
    {
        name: 'Rebeca',
        course: 'java',
        age: 25,
    },
    {
        name: 'Sara',
        course: 'python',
        age: 16,
    },
    {
        name: 'Manu',
        course: 'python',
        age: 38,
    },
    {
        name: 'Ricardo',
        course: 'js',
        age: 25,
    },
];

// Crea un nuevo array donde solo esten incluídos los estudiantes que están cursando JavaScript ("js").
const onlyJs = [];

for (const js of students) {
    if (js.course === 'js') {
        onlyJs.push(js);
    }
}

console.log(onlyJs);

// Crea un nuevo array donde solo esten incluídos los estudiantes menores de 30 años.
const onlyMoreThan30Years = [];

for (const check of students) {
    if (check.age <= 30) {
        onlyMoreThan30Years.push(check);
    }
}

console.log(onlyMoreThan30Years);

// Agrega a todos los estudiantes del array original la propiedad "isAdult". Deberá ser true o false en función de la edad.
for (let i = 0; i < students.length; i++) {
    students[i].isAdult = students[i].age > 17;
}

console.log(students);
