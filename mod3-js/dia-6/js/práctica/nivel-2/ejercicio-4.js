/**
 * #################
 * ## Ejercicio 4 ##
 * #################
 *
 * Ordena el siguiente array numérico de menor a mayor: [4, 10, 7, 1, 2]
 *
 * ¡No se puede usar el método sort()!
 *
 */

'use strict';

const nums = [5, 6, 3, 2, 8];

const arrayOrder = (numArray) => {
    const newArray = [];
    for (let i = 0; i < numArray.length; i++) {
        let minimum = Math.min.apply(null, numArray);
        newArray.push(minimum);
        numArray[numArray.indexOf(minimum)] =
            minimum + Math.max.apply(null, numArray);
    }
    return console.log(newArray);
};

arrayOrder(nums);
