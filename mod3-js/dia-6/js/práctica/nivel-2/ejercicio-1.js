/**
 * #################
 * ## Ejercicio 1 ##
 * #################
 *
 * Dado un array de frutas, genera un nuevo objeto en el que cada fruta pase a ser una
 * nueva propiedad del objeto. El valor asignado a esta propiedad debe ser el nº de veces
 * que la fruta se repite en el array.
 *
 *      const fruitBasket = ['naranja', 'naranja', 'limón', 'pera', 'limón', 'plátano', 'naranja'];
 *
 * Para el array anterior, el objeto resultante debería ser:
 *
 *      const fruits = {
 *          naranja: 3,
 *          limón: 2,
 *          pera: 1,
 *          plátano: 1
 *      };
 */

'use strict';

const fruitBasket = [
    'naranja',
    'naranja',
    'limón',
    'pera',
    'limón',
    'plátano',
    'naranja',
];

//Objeto vacío para añadir propiedades y valores
const result = {};

//recorrer el array de frutas
for (const fruit of fruitBasket) {
    //comprobamos si la propiedad existe
    if (fruit in result) {
        //si la propeidad existe sumamos 1. Accedemos al objeto y le sumamos 1 al valor
        result[fruit]++;
    } else {
        //si la propiedad no existe se crea y le agregamos 1. Despues al existir ya le agrega 1 en el código inicial.
        result[fruit] = 1;
    }
}

console.log(result);
