/**
 * #################
 * ## Ejercicio 3 ##
 * #################
 *
 * Crea una función que interprete el contenido entre paréntisis de
 * un string dado.
 *
 *      - El programa debe devolver el texto rodeando por paréntisis.
 *
 *      - Si no hay paréntisis se devolverá un string vacío.
 *
 *      - Si existe paréntisis de apertura pero no de cierre se devolverá
 *        el contenido desde el primer paréntisis hasta el final del string.
 *
 *      - Si existe paréntisis de cierre pero no de apertura se devolverá
 *        el contenido desde el inicio hasta el paréntisis de cierre.
 *
 * Por ejemplo, si el string fuera "Hola (que) tal" se mostrará el "que".
 *
 * Si fuera "Hola (que tal" se devolvería "que tal".
 *
 */

'use strict';

const parentesis = () => {
    let userWord = prompt('Introduce tu frase con ()');
    const openParenthesis = userWord.indexOf('(');
    const closeParenthesis = userWord.indexOf(')');
    let newUserWord = '';
    if (openParenthesis !== -1 && closeParenthesis !== -1) {
        for (let i = openParenthesis + 1; i < closeParenthesis; i++) {
            newUserWord += userWord[i];
        }
    } else if (openParenthesis != -1 && closeParenthesis === -1) {
        for (let i = openParenthesis + 1; i < userWord.length; i++) {
            newUserWord += userWord[i];
        }
    } else if (openParenthesis === -1 && closeParenthesis !== -1) {
        for (let i = 0; i < closeParenthesis; i++) {
            newUserWord += userWord[i];
        }
    } else {
        console.log('No se ha encontrado ningún caracter de paréntesis');
    }
    console.log(newUserWord);
};

parentesis();
