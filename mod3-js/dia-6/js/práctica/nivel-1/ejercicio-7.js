/**
 * #################
 * ## Ejercicio 7 ##
 * #################
 *
 * Crea una función que pida una cadena de texto y la devuelva al revés.
 * Es decir, si tecleo "hola que tal" deberá mostrar "lat euq aloh".
 *
 */

'use strict';

const reverseString = () => {
    let randomWord = [...prompt('Introduce una palabra')].reverse().join('');
    alert(`Al revés! ${randomWord}`);
};

reverseString();

//También con split() en lugar de con ...
