/**
 * #################
 * ## Ejercicio 8 ##
 * #################
 *
 * A partir del string dado crea un array en el que cada una de las distintas palabras
 * (palabras, no letras) del string sea una posición del array.
 *
 *  - No debe haber letras mayúsculas.
 *
 *  - El array no debe contener signos de puntuación, SOLO LETRAS.
 *
 *  - El array debe estar ordenado por orden alfabético inverso, ¿método "sort"? ;)
 *
 * Resultado esperado: ["sit", "lorem", "ipsum", "elit", "dolor", "consectetur", "amet", "adipisicing"]
 *
 */

'use strict';

const text = '¡Lorem Ipsum Dolor Sit Amet, Consectetur Elit Adipisicing!';

const transforming = text
    .toLocaleLowerCase()
    .replaceAll(',', '')
    .replaceAll('!', '')
    .replaceAll('¡', '')
    .split(' ')
    .sort((a, b) => (a > b ? -1 : 1));

// const myArray = transforming.split(' ').sort((a, b) => (a > b ? -1 : 1));

console.log(transforming);

//Podemos usar sort().reverse() y ya estaría en lugar de darle valores a sort()
