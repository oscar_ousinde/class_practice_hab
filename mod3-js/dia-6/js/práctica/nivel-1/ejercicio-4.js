/**
 * #################
 * ## Ejercicio 4 ##
 * #################
 *
 * Crea una función que reciba como parámetro un array cualquiera y retorne
 * otro array con los elementos del primero en orden inverso.
 *
 * ¡No vale utilizar el método "reverse"!
 *
 */

'use strict';

const colors = ['blue', 'yellow', 'black', 'pink'];

const arrayReturn = (randomArray) => {
    const newArray = [];
    for (let i = randomArray.length - 1; i >= 0; i--) {
        newArray.push(randomArray[i]);
    }
    console.log(newArray);
};

arrayReturn(colors);

//También se puede hacer return del nuevo array en lugar de un console.log
