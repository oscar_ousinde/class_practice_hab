/**
 * #################
 * ## Ejercicio 5 ##
 * #################
 *
 * Crea una función que reciba una cadena de texto y la muestre poniendo
 * el signo "–" entre cada carácter sin usar el método replace ni replaceAll.
 *
 * Por ejemplo, si tecleo "hola qué tal", deberá salir "h-o-l-a- -q-u-e- -t-a-l".
 *
 */

'use strict';

const add = (randomWord) => {
    let newWord = '-';
    for (const letters of randomWord) {
        newWord += letters + '-';
    }
    return newWord;
};

console.log(add('Oscar Ousinde'));

//También se puede utilziar el método split() y join()
function generateString(str) {
    const arr = str.split('').join('-');
    return arr;
}

console.log(generateString('Hola que tal?'));
