/**
 * #################
 * ## Ejercicio 1 ##
 * #################
 *
 * Dado el array [3, 4, 13, 5, 6, 8], muestra por consola qué numeros son pares
 * y qué números son impares.
 *
 * Por último, crea un nuevo array en el que los valores sean el doble del array
 * original.
 *
 */

'use strict';

const nums = [3, 4, 13, 5, 6, 8];

// * Dado el array [3, 4, 13, 5, 6, 8], muestra por consola qué numeros son pares
// * y qué números son impares.
//Podemos crear una función para comprobar que un nñumero sea par o impar y después
//llamar la función dentro del for of con un if. If true --> es par!
for (const num of nums) {
    if (num % 2 === 0) console.log(num);
}

// * Por último, crea un nuevo array en el que los valores sean el doble del array
// * original.

let newNums = [];

for (const num of nums) {
    newNums.push(num * 2);
}

console.log(newNums);
