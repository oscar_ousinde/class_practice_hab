/* #################
 * ## Ejercicio 6 ##
 * #################
 *
 * - Cuenta el número de letras "r" en el siguiente fragmento de texto:
 *   "Tres tristes tigres tragan trigo en un trigal."
 *
 * - Ahora cuenta las "t". Debes contar las mayúsculas y las minúsculas.
 *
 * - Sustituye todas las "e" por "i".
 *
 */

'use strict';

const text = 'Tres tristes tigres tragan trigo en un trigal.';

// Cuenta el número de letras "r" en el siguiente fragmento de texto:
// "Tres tristes tigres tragan trigo en un trigal."
let rs = 0;
for (const lettersR of text) {
    if (lettersR === 'r') {
        rs++;
    }
}
console.log(`Letters 'r': ${rs}`);

// Ahora cuenta las "t". Debes contar las mayúsculas y las minúsculas.
let ts = 0;
let tsMayus = 0;

for (const letters of text) {
    if (letters === 't') {
        ts++;
    } else if (letters === 'T') {
        tsMayus++;
    }
}
console.log(`Letters 't': ${ts}`);
console.log(`Letters 'T': ${tsMayus}`);

// Sustituye todas las "e" por "i".
console.log(text.replaceAll('e', 'i'));
