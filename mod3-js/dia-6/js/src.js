'use strict';

//MÉTODOS CON STRING
const text = 'Métodos con strings';

//Longitud string
console.log(text.length);

//Generar nuevo string con mayúsculas
console.log(text.toLocaleUpperCase());

//Generar nuevo string con minúsculas
console.log(text.toLocaleLowerCase());

//Encontrar posición de un caracter o conjunto de caracteres. Devuelve la primera coincidencia
//Si el caracter no existe retorna el valor numérico -1.
//Con el conjunto de cracteres que busquemos nos devuelve la posición del primer carácter del string
//Existe un método similar que recorre el array de la última posición a la primera lastIndexOf().
console.log(text.indexOf('o'));
console.log(text.indexOf('string'));

//Repetir X veces una variable
console.log(text.repeat(3));

//Replace. Permite reemplazar un caracter o conjunto de caracteres por otro/s sting.
//Solo reemplaza la prmiera coincidencia de izquierda a derecha. No lo reemplaza, retorna un nuevo string con la coincidencia reemplazada
console.log(text.replace('o', 'i'));
console.log(text.replace('string', 'array'));

//El replaceAll permite reemplazar todas las coincidencias
console.log(text.replaceAll('o', 'i'));

//El método split me permite crear un array utilizando como referencia los caracteres de un string
//Utiliza el caracter que indicamos como divisor. En este caso con cada letra.
console.log(text.split(''));
console.log(text.split(' '));
console.log('22:30'.split(':'));

//El metodo slice permite obtener una parte concreta de u string
//La posición 12 en este ejemplo es excluyente
console.log(text.slice(8));
console.log(text.slice(8, 12));

//El metodo includes retorna True si el caracter o conjunto de caracteres existe dentro del string
console.log(text.includes('o')); //True
console.log(text.includes('w')); //False
console.log(text.includes('string')); //True
