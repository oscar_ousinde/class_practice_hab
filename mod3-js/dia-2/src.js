'use strict';

//EMPEZANDO JS
/* const username = 'oscar93';
let age = 29;
let isAdult = age > 29; */

//Variable inicializada pero no asignada. Da undefined como typeof. Se puede dejar sin asignar solo let nunca una const
let favNumber;
console.log(typeof favNumber);

//Error de JS!!. Debería ser tipo null no tipo object
let box = null;
console.log(typeof box);

//OPERACIONES MATEMÁTICAS
console.log(2 ** 3); //potencia de un número
console.log(8 % 2); //resto de una división

//OPERADOR INCREMENTO
//aumenta o disminuye valores de 1 en 1
let numIncrement = 10;
numIncrement++;
console.log(numIncrement);

let numDecrement = 10;
numDecrement--;
console.log(numDecrement);

let num1 = 10;
num1 += 10; // es igual a num2 = num2 +10;
console.log(num1);

let num2 = 10;
num2 -= 10; // es igual a num2 = num2 -10;
console.log(num2);

//PUERTAS LÓGICAS Y SUS OPERADORES
console.log(1 > 0);
console.log(1 < 0);
console.log(1 >= 0);
console.log(1 <= 0);
console.log(1 === 0); //igualdad estricta. No usar el ==, son malas prácticas
console.log(1 !== 0); //distinto de. Desigualdad estricta

//and, or y not
console.log(2 > 1 && 5 > 1); //--> si una condición no se cumple es false. Operador AND &&
console.log(2 > 1 || 5 > 10); //--> si una condición no se cumple es true. Operador OR ||
console.log(2 > 1 !== 5 > 1); //--> Operador NOT !. Dos operadores NOT se anulan.
