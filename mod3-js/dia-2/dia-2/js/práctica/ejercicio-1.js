/* #################
 * ## Ejercicio 1 ##
 * #################
 *
 * Crea una variable que almacene tu nombre y otra variable que almacene
 * tu número favorito.
 *
 *  - Muestra ambos valores por consola.
 *
 *  - Cambia tu nº favorito por cualquier otro nº.
 *
 *  - Muestra por consola el resultado del cambio.
 *
 *  - Muestra por consola el tipo de las variables definidas.
 *
 */

'use strict';

const username = 'David';
let favoriteNum = 14;

console.log(username, favoriteNum);

favoriteNum = 7;

console.log(favoriteNum);

console.log('La variable [username] es de tipo ' + typeof username);
console.log('La variable [favoriteNum] es de tipo ' + typeof favoriteNum);
