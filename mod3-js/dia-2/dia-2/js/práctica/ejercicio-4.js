/* #################
 * ## Ejercicio 7 ##
 * #################
 *
 * Crea una calculadora con "switch" que opere con dos números. La calculadora
 * utilizará la variable "option" para decidir el tipo de operación a realizar.
 * Debe poder sumar, restar, multiplicar y dividir. A mayores debe permitir
 * elevar el nº A a la potencia de B.
 *
 * Bonus point: muestra un mensaje de error en caso de que el número B sea 0
 *              y se este realizando una división (no se puede dividir entre 0).
 *
 */

'use strict';

/**
 *  0 es suma
 *  1 es resta
 *  2 es multiplicación
 *  3 es división
 *  4 es la potencia de [a] sobre [b]
 */

/* const a = 4;
const b = 0;
const option = 0;

switch (option) {
    case 0:
        console.log(a + b);
        break;
    case 1:
        console.log(a - b);
        break;
    case 2:
        console.log(a * b);
        break;
    case 3:
        if (b === 0) {
            throw new Error('b no puede ser cero');
        } else {
            console.log(a / b);
        }
        break;
    case 4:
        console.log(a ** b);
        break;
    default:
        console.error('Algo ha salido mal!');
} */

//CON OPERADOR TERNARIO APARECE UN ERROR. NO SE PUEDE USAR AHÍ UN OPERADOR TERNARIO
/* const a = 4;
const b = 0;
const option = 3;

switch (option) {
    case 0:
        console.log(a + b);
        break;
    case 1:
        console.log(a - b);
        break;
    case 2:
        console.log(a * b);
        break;
    case 3:
        b === 0 ? throw new Error('b no puede ser cero') : console.log(a / b);
        break;
    case 4:
        console.log(a ** b);
        break;
    default:
        console.error('Algo ha salido mal!');
} */

//EL OPERADOR TERNARIO SI FUNCIONA...
/* const numberOne = 1;
const numberZero = 0;

numberOne > numberZero ? console.log('Correcto') : console.log('Incorrecto'); */

//OTRA FORMA USANDO OPERADORES COMO strings '+' , '-' , '*'...
const a = 4;
const b = 0;
const option = '/';

switch (option) {
    case '+':
        console.log(`${a} + ${b} = ${a + b}`);
        break;
    case '-':
        console.log(`${a} - ${b} = ${a - b}`);
        break;
    case '*':
        console.log(`${a} * ${b} = ${a * b}`);
        break;
    case '/':
        if (b === 0) {
            throw new Error('b no puede ser cero');
        } else {
            console.log(`${a} / ${b} = ${a / b}`);
        }
        break;
    case '^':
        console.log(`${a} ^ ${b} = ${a ** b}`);
        break;
    default:
        console.error('Algo ha salido mal!');
}
