/* #################
 * ## Ejercicio 3 ##
 * #################
 *
 * Crea una calculadora con "if" que opere con dos números. La calculadora
 * utilizará la variable "option" para decidir el tipo de operación a realizar.
 * Debe poder sumar, restar, multiplicar y dividir. A mayores debe permitir
 * elevar el nº A a la potencia de B.
 *
 * Bonus point: muestra un mensaje de error en caso de que el número B sea 0
 *              y se este realizando una división (no se puede dividir entre 0).
 *
 */

'use strict';

/**
 *  0 es suma
 *  1 es resta
 *  2 es multiplicación
 *  3 es división
 *  4 es la potencia de [a] sobre [b]
 */

const a = 4;
const b = 2;
const option = 3;

if (option === 0) {
    console.log(a + b);
} else if (option === 1) {
    console.log(a - b);
} else if (option === 2) {
    console.log(a * b);
} else if (option === 3 && b === 0) {
    throw new Error('La variable [b] no puede ser cero');
} else if (option === 3) {
    console.log(a / b);
} else if (option === 4) {
    console.log(a ** b);
} else {
    console.log('Error');
}

//OTRA FORMA DE HACERLO POR TENER CADA IF UNA SOLA OPCIÓN PERO POCO LEGIBLE
/* const a = 4;
const b = 2;
const option = 3;

if (option === 0) {
    console.log(a + b);
} else if (option === 1) console.log(a - b);
else if (option === 2) console.log(a * b);
else if (option === 3 && b === 0)
    throw new Error('La variable [b] no puede ser cero');
else if (option === 3) console.log(a / b);
else if (option === 4) console.log(a ** b);
else console.log('Error'); */
