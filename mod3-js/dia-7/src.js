'use strict';

const sum = (a, b) => a + b;
const sub = (a, b) => a - b;

function calculate(a, b, myCallback) {
    return myCallback(a, b);
}

console.log(calculate(5, 6, sum)); //en lugar de incluir sum se puede crear aquí dentro en lugar de sum

//Vamos a utilizar metodos con CALLBACKS como el sort(). Pasar función como argumento de sort()
//.sort(callbackFunction). Sort modifica el array original!!CUIDADO!
const nums = [12, 6, 118, 45, 1, 7];

console.log(nums.sort((a, b) => a - b));

//Generamos un array cuyos valores serán el doble que el array original
//Vamos a usar map! La función que le pasamos al map(el callback tiene 3 parámetros inmutables donde el más importante es el primero)
//El primer parámetro de un map es equivalente a la constante del for of. Es el valor
const double = nums.map((num) => num * 2);

console.log(double);

//Con arrays de objetos también podemos usar el .map()
//Vamos a agregar la propeidad isAdult

const students = [
    {
        name: 'Carla',
        age: 37,
    },
    {
        name: 'Jose',
        age: 17,
    },
    {
        name: 'Pablo',
        age: 47,
    },
];

const newStudents = students.map((student) => {
    //Agregar la nueva propiedad
    student.isAdult = student.age > 17;

    //Retornar student
    return student;
});

console.table(newStudents);

//Filtrar estudiantes mayores de edad
//Podemo susar .filter(). No usar map porque da un array con el mismo número de elementos que el array original! No sirve
//Filter funciona idéntico a map! Pero devuelve siempre valores booleanos

const olderStudents = students.filter((student) => {
    return student.age > 17;
});

console.table(olderStudents);

//El método .reduce(). Permite reducir elementos de un array a un solo elemento
//El primer parámetro del callback del reduce es un valor considerado como el acumulador
//En ese acumulador se suman los números del array es donde se acumulan. El segundo parámetro del callback es el elemento del array a recorrer y el tercero su índice (opcional)
//El callback es el primer argumento del método reduce. El segundo argumento opcional podemos pasar el valor inicial del acumulador!
//Si no damos un segundo argumento donde comienza el acumulador, este empieza en la posición 1 del array y no el la 0 pero toma su valor para acumularlo.

//Suma total de todos los nñumeros del array. Del array de numeros nums
const total = nums.reduce((acc, num) => {
    return (acc += num);
}, 100);
console.log(total);

//Vamos a sumar todas las edades de los estudiantes
const totalAges = students.reduce((acc, student) => {
    return (acc += student.age);
}, 0);

console.log(totalAges);
