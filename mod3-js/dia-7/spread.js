'use strict';

/**###################
 * ##Spread Operator##
 *####################
 */

const colors = ['yellow', 'black', 'purple', 'red'];

console.log(colors);

//Este operador ... tiene dos funciones (Spread). Permite acceder al contenido de un array u objeto.
//La otra función es el rest
console.table(...colors);

//Ejemplo con rest. Pasar múltiples argumentos a un mismo parametro. Devuelve array con todos los valores en los que se peude iterar.
const sum = (...nums) => {
    let total = 0;
    for (const num of nums) {
        total += num;
    }
    return total;
};

console.log(sum(34, 2, 6, 8));

//Ejemplo
//Usamos spread como ... con el array colors para añadirlo al nuevo array. Copia del interior de array u objeto
const moreColors = ['white', 'blue', ...colors];
console.log(moreColors);

//Con objetos
//Incluso hemos cambiado un valor de un propiedad de Lucia por Paco.
const Person = {
    name: 'Lucía',
    age: 29,
    favouriteColor: 'blue',
};

const newPerson = {
    ...Person,
    name: 'Paco',
    job: 'Plumber',
};

console.table(newPerson);

/**
 * Al utilizar el spread con arrays u objetos se hace una copia poco profunda
 * y siempre que modifiquemos ese array u objeto se modifica el array u objeto original!
 * en este último caso si tuviesemos un array como valor de un propiedad de un objeto
 * e hiciésemos un spread de todo el objeto, si modificamos el array de la copia de spread
 * se modificará también el array original de donde procede la copia!
 */
