/**
 * #################
 * ## Ejercicio 4 ##
 * #################
 *
 * Utiliza los métodos map, filter o reduce para resolver las siguientes propuestas:
 *
 *  1. Obtén la suma total de todas las edades de las personas.
 *  2. Obtén la suma total de todas las edades de las personas francesas.
 *  3. Obtén un array con el nombre de todas las mascotas.
 *  4. Obtén un array con las personas que tengan gato.
 *
 */

'use strict';

const persons = [
    {
        name: 'Berto',
        country: 'ES',
        age: 44,
        pet: {
            name: 'Moon',
            type: 'perro',
        },
    },
    {
        name: 'Jess',
        country: 'UK',
        age: 29,
        pet: {
            name: 'Kit',
            type: 'gato',
        },
    },
    {
        name: 'Tom',
        country: 'UK',
        age: 36,
        pet: {
            name: 'Rex',
            type: 'perro',
        },
    },
    {
        name: 'Alexandre',
        country: 'FR',
        age: 19,
        pet: {
            name: 'Aron',
            type: 'gato',
        },
    },
    {
        name: 'Rebeca',
        country: 'ES',
        age: 32,
        pet: {
            name: 'Carbón',
            type: 'gato',
        },
    },
    {
        name: 'Stefano',
        country: 'IT',
        age: 52,
        pet: {
            name: 'Bimbo',
            type: 'perro',
        },
    },
    {
        name: 'Colette',
        country: 'FR',
        age: 22,
        pet: {
            name: 'Amadeu',
            type: 'gato',
        },
    },
];

// 1. Obtén la suma total de todas las edades de las personas.
const totalAges = persons.reduce((acc, person) => {
    return (acc += person.age);
}, 0);
console.log(totalAges);

// 2. Obtén la suma total de todas las edades de las personas francesas.
const totalAgesFrench = persons.reduce((acc, person) => {
    if (person.country === 'FR') acc += person.age;
    return acc;
}, 0);
console.log(totalAgesFrench);

// 3. Obtén un array con el nombre de todas las mascotas.
const pets = persons.map((person) => {
    return person.pet.name;
});
console.log(pets);

// 4. Obtén un array con las personas que tengan gato.
const cat = persons.filter((person) => {
    return person.pet.type === 'gato';
});
console.log(cat);
