'use strict';

const nums = [14, 6, 118];

// Ordenamos los elementos del array pasándole al método sort
// una función de comparación. Para ordenar de mayor a menor
// restamos b - a. ¡MODIFICA EL ARRAY ORIGINAL!
// nums.sort((a, b) => a - b);

// Generamos un array cuyos valores serán el doble que
// el array original.
const double = nums.map((num) => num * 2);

const students = [
    {
        name: 'Carla',
        age: 37,
    },
    {
        name: 'Jose',
        age: 17,
    },
    {
        name: 'Pablo',
        age: 47,
    },
];

// Agregamos al array de estudiantes la propiedad isAdult.
const newStudents = students.map((student) => {
    // Agregamos la nueva propiedad.
    student.isAdult = student.age > 17;

    // Retornamos al estudiante.
    return student;
});

// Filtramos a los estudiantes mayores de edad.
const olderStudents = students.filter((student) => {
    return student.age > 17;
});

// Suma total de todos los números del array.
const total = nums.reduce((acc, num, i) => {
    // console.log(`Posición ${i} --> acc = ${acc}`);
    // console.log(`acc = ${acc} + ${num}`);
    // console.warn('');
    return (acc += num);
});

// Sumar todas las edades de los estudiantes.
const totalAges = students.reduce((acc, student) => {
    return (acc += student.age);
}, 0);

console.log(totalAges);
