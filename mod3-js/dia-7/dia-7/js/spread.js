'use strict';

/**
 * #####################
 * ## Spread Operator ##
 * #####################
 *
 * Permite hacer copias POCO PROFUNDAS de un array o un
 * objeto.
 *
 */

const colors = ['yellow', 'black', 'purple', 'red'];

// Creamos un uevo array con dos colores sumando los colores
// del array de la linea 13.
const moreColors = ['white', 'blue', ...colors];

console.table(moreColors);

const Person = {
    name: 'Lucía',
    age: 29,
    favoriteColor: 'blue',
};

// Copiamos todas las propiedades de Person y sobreescribimos
// el nombre,
const newPerson = {
    ...Person,
    name: 'Paco',
};

console.table(newPerson);
