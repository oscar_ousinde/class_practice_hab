'use strict';

/**
 * TIP: podemos cambiar el nombre de una variable
 * en TODO el código seleccionandola, pulsando F2
 * y se cambia en todo el código!!
 */

const colores = ['amarillo', 'blanco', 'violeta'];

//console.table nos da por consola una tabla.
//Es útil para ver arrays y objetos!
console.table(colores);

//Acceder a una posición concreta
console.table(colores[1]);

//Modificamos una posición
colores[1] = 'negro';
console.table(colores);

//Usando operadores con las posiciones del array
const nums = [1, 2, 3, 4, 5, 6, 8];
nums[1]++;
console.table(nums);

//Añadir un elemento al final del array .push()
//Lo normal es 'pushear' un elemento. Pero se puede hacer con varios
nums.push(111, 222, 333);
nums.push(3 * 5);
console.table(nums);

//Saber la longitud de un array .lenght
console.log('La longitud del array es:' + nums.length);

/**
 * Recorrer arrays!
 * Mostrar por consola todos los valores del array
 */

const numbers = [1, 2, 3, 4, 5];

for (let i = 0; i < numbers.length; i++) {
    console.log(numbers[i]); //aquí podemos añadir operadores!
}

//Con esto no modificamos el array original. Para eso tendriamos que
//utilizar en el cuerpo del for numbers[i] = numbers[i] * 2

//El bucle for of nos sirve para recorrer arrays
//La constante se llame como sea va a ir iterando los elementos del array. No hay condición

for (const value of numbers) {
    console.log(value);
}
