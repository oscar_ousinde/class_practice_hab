'use strict';
//OBJETOS
//declarados con UpperCamelCase.
//Un objeto vacío es considerado true.

const Person = {
    name: 'Paula',
    age: 27,
    favColor: 'red',
    pet: {
        name: 'Firulais',
    },
    sayHello: function () {
        console.log(`Soy ${Person.name}`);
    },
};
console.log(Person);

//acceder a los valores de las propiedades de un objeto
//notación por punto
console.log(Person.age);
console.log(Person.pet.name);

//notación por corchetes
//se deben usar corchetes para llamar con corchetes
console.log(Person['pet']['name']);

//modificar valores
Person.pet.name = 'Fada';
Person.age++;

//borrar propiedad
delete Person.favColor;
console.log(Person.favColor); //es undefined porque no existe. Ha sido borrada.

//comprobar si existe una propiedad dentro de un objeto
//con el operador in
if ('name' in Person) {
    console.log('Existe la propiedad [name]');
} else {
    console.log('No existe la propiedad [name]');
}

//obtener lista de propiedades (keys) y valores en un array
const keys = Object.keys(Person);
console.log(keys);

const values = Object.values(Person);
console.log(values);

//MÉTODOS
//es una función asignada como valor a una propiedad de un objeto
Person.sayHello();
