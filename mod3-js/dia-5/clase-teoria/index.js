'use strict';

//Nan es valor falso o cero. En esta condición por defecto se ejecuta el else
//Si introducimos el !Nan se ejecuta la primera condición
//Para ver si un valor es falso console.log(Boolean(valorAEvaluar))
/* if (!NaN) {
    console.log('Hola');
} else {
    console.log('Adiós');
} */

/**
 * JS toma un string vacío como valor falso.
 * En este caso JS comprueba si la variable es true o false en el if.
 * string vacía -> false, string no vacía -> true.
 * valor cero -> false, no cero -> true
 * undefined + null -> false
 */
/* const username = '';
if (username) {
    console.log('Existe nombre de usuario');
} else {
    console.log('No existe nombre de usuario');
} */
