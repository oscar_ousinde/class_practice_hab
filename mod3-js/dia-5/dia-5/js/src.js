'use strict';

/* let age = 17;

// Operador ternario.
let message = age > 17 ? 'Mayor de edad' : 'Menor de edad';

console.log(message); */

/**
 * ##################
 * ## Falsy Values ##
 * ##################
 */
/* 
// Número 0.
console.log(Boolean(0));

// String vacío.
console.log(Boolean(''));

// Null.
console.log(Boolean(null));

// Undefined.
console.log(Boolean(undefined));

// NaN.
console.log(Boolean(NaN));
 */

const Person = {
    name: 'Paula',
    age: 27,
    favColor: 'purple',
    pet: {
        name: 'Firulais',
    },
    sayHello: function () {
        console.log(`Soy ${Person.name}`);
    },
};

Person.sayHello();

// Modificar propiedades de un objeto.
Person.pet.name = 'Berritxu';
Person.age += 4;

// Notación por punto.
console.log(Person.pet.name);

// Notación por corchetes.
console.log(Person['age']);

// Borrar una propiedad.
delete Person.favColor;

// Compruebo que haya eliminado el color.
console.log(Person.favColor);

// Comprobar si existe una propiedad dentro de un objeto.
if ('name' in Person) {
    console.log('Existe la propiedad [name].');
} else {
    console.log('No existe la propiedad [name].');
}

// Obtenemos un array con la lista de propiedades de un objeto.
const keys = Object.keys(Person);

console.log(keys);

// Obtenemos un array con los valores de un objeto.
const values = Object.values(Person);

console.log(values);
