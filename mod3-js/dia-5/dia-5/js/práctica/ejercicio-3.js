/* #################
 * ## Ejercicio 3 ##
 * #################
 *
 * Dado el array = [1, 3, 9, 14, 17, 22]
 *
 *  - Iterar por todos los elementos dentro de un array utilizando "while" y mostrarlos en pantalla.
 *
 *  - Iterar por todos los elementos dentro de un array utilizando "for" y mostrarlos en pantalla.
 *
 *  - Iterar por todos los elementos dentro de un array utilizando "for of" y mostrarlos en pantalla.
 *
 *  - Mostrar todos los elementos dentro de un array sumándole uno a cada uno.
 *
 *  - Generar otro array con todos los elementos del primer array incrementados en 1. ¿Con el método push?
 *
 *  - Calcular el promedio.
 *
 */

'use strict';

const nums = [1, 3, 9, 14, 17, 22];

// Iterar por todos los elementos dentro de un array utilizando "while" y mostrarlos en pantalla.
let contador = '0';
while (contador < nums.length) {
    console.log(nums[contador]);
    contador++;
}

// Iterar por todos los elementos dentro de un array utilizando "for" y mostrarlos en pantalla.
for (let i = 0; i < nums.length; i++) {
    console.log(nums[i]);
}

// Iterar por todos los elementos dentro de un array utilizando "for of" y mostrarlos en pantalla.
for (const index of nums) {
    console.log(index);
}

// Mostrar todos los elementos dentro de un array sumándole uno a cada uno.
/* for (let i = 0; i < nums.length; i++) {
    nums[i]++;
    console.log(nums[i]);
} */
/* for (const index of nums) {
    console.log(index + 1);
} */

// Generar otro array con todos los elementos del primer array incrementados en 1. ¿Con el método push?
const newArray = [];
for (let i = 0; i < nums.length; i++) {
    nums[i]++;
    newArray.push(nums[i]);
}
console.log(newArray);

const nums2 = [];
for (const num of nums) {
    nums2.push(num + 1);
}
console.log(nums2);

// Calcular el promedio.
let mean = 0;
for (let i = 0; i < nums.length; i++) {
    mean += nums[i];
}

console.log(mean / nums.length);

//Otra forma de calcula la media
let total = 0;
for (const num of nums) {
    total += num;
}
console.log(total / nums.length);
