/* #################
 * ## Ejercicio 1 ##
 * #################
 *
 * - Crea el objeto coche y asígnale las propiedades modelo, marca y color.
 *
 * - Muestra el objeto por medio de un "console.log".
 *
 * - Modifica el valor de la propiedad color y agrega la propiedad año de
 *   matriculación.
 *
 * - Crea un método que muestre por consola un mensaje indicando el modelo
 *   y el color del coche.
 *
 * - Utiliza un "confirm" para mostrar por consola las propiedades, o los
 *   valores. Si la persona acepta el "confirm" se mostrarán las propiedades,
 *   de lo contrario, se mostrarán los valores.
 *
 */

'use strict';

const Car = {
    model: 'Supra',
    brand: 'Toyota',
    color: 'black',
    modelAndColor: function () {
        console.log(`This car is a ${Car.model} and color ${Car.color}`);
    },
};

Car.color = 'Purple';
Car.plateYear = '1999';

//crear un metodo indirectamente
Car.brandAndModel = function () {
    `El coche es un ${Car.brand} ${Car.model}`;
};

Car.modelAndColor();

if (confirm('Acepta para mostrar propiedades o Cancela para mostrar valores')) {
    console.log(Object.keys(Car));
} else {
    console.log(Object.values(Car));
}

console.log(Car);
