/**
 * #################
 * ## Ejercicio 2 ##
 * #################
 *
 * ¿Te suena el juego piedra, papel o tijera? Pues manos a la obra.
 *
 *      - Tu oponente será la computadora.
 *
 *      - El ganador se decide al mejor de 3.
 *
 *      - Almacena la puntuación del jugador y de la computadora
 *        en el objeto Game como propiedades del mismo. Ejemplo:
 *
 *          const Game = {
 *              player: 0,
 *              computer: 0
 *          }
 *
 */

'use strict';

const Game = {
    player: 0,
    computer: 0,
};

while (Game.player < 3 && Game.computer < 3) {
    let user = Number(prompt('¿Piedra (1), Papel (2) o Tijera (3?'));
    let computer = Math.ceil(Math.random() * 3);
    switch (computer) {
        case 1:
            if (user === 1) {
                console.log(`Empate! Computer: PIEDRA vs Player: PIEDRA`);
            } else if (user === 2) {
                console.log(`Has ganado! Computer: PIEDRA vs Player: PAPEL`);
                Game.player++;
            } else {
                console.log(`Pierdes! Computer: PIEDRA vs Player: TIJERA`);
                Game.computer++;
            }
            break;
        case 2:
            if (user === 1) {
                console.log(`Pierdes! Computer: PAPEL vs Player: PIEDRA`);
                Game.computer++;
            } else if (user === 2) {
                console.log(`Empate! Computer: PAPEL vs Player: PAPEL`);
            } else {
                console.log(`Has ganado! Computer: PAPEL vs Player: TIJERA`);
                Game.player++;
            }
            break;
        case 3:
            if (user === 1) {
                console.log(`Has ganado! Computer: TIJERA vs Player: PIEDRA`);
                Game.player++;
            } else if (user === 2) {
                console.log(`Pierdes! Computer: TIJERA vs Player: PAPEL`);
                Game.computer++;
            } else {
                console.log(`Empate! Computer: TIJERA vs Player: TIJERA`);
            }
            break;
    }
}

console.log(`RESULTS: computer ${Game.computer}, player: ${Game.player}`);
