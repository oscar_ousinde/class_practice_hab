// Seleccionamos los nodos con los que vamos a trabajar.
const todoList = document.querySelector('ul.todo-list');
const todoForm = document.querySelector('form.todo-form');

/**
 * ####################
 * ##  LocalStorage  ##
 * ####################
 *
 * Obtenemos las tareas almacenadas en el Local Storage (si las hay).
 *
 */

const localStorageTasks = JSON.parse(localStorage.getItem('tasks'));

/**
 * #############
 * ##  State  ##
 * #############
 *
 * Si queremos obtener varios elementos en el localStorage es recomendable
 * guardar esos valores en las propiedades de un objeto, en este caso, State.
 *
 * Si en la constante anterior recibimos un valor null, JavaScript se tomará
 * eso como algo falso (falsy values). En ese caso nos quedaríamos con un array
 * vacío.
 *
 * Si localStorageTasks tiene un array de tareas nos quedamos con ese array.
 *
 */

const State = {
    tasks: localStorageTasks || [
        {
            text: 'Pasear al perro',
            done: false,
            date: '2021-05-12T15:53:18.507Z',
        },
        {
            text: 'Hacer la cena',
            done: true,
            date: '2021-05-12T15:53:18.507Z',
        },
        {
            text: 'Repasar JavaScript',
            done: false,
            date: '2021-05-12T15:53:18.507Z',
        },
    ],
};

/**
 * ##############################
 * ##  Enviar una nueva tarea  ##
 * ##############################
 *
 * Agregamos una función manejadora al evento submit del formulario que se
 * encargue de agregar una nueva tarea con los datos que el usuario introdujo
 * en el input.
 *
 * Renderiza la página tras enviar una nueva tarea.
 *
 */

todoForm.addEventListener('submit', (e) => {
    // Prevenimos el comportamiento por defecto del formulario.
    e.preventDefault();

    // Seleccionamos el input con nombre "todo".
    const input = todoForm.elements.todo;

    // Creamos una nueva tarea con el texto del input.
    State.tasks.push({
        text: input.value,
        done: false,
        date: new Date().toISOString(),
    });

    // Guardamos el array en el localStorage.
    localStorage.setItem('tasks', JSON.stringify(State.tasks));

    // Vaciamos el input.
    input.value = '';

    // Renderizamos la página.
    render();
});

/**
 * ##################################################
 * ##  Marcar tarea como realizada / no realizada  ##
 * ##################################################
 *
 * Agrega un evento al ul que marque una tarea como realizada o
 * no realizada.
 *
 * Renderiza la página tras marcar / desmarcar las tareas.
 *
 */

todoList.addEventListener('click', (e) => {
    // Seleccionamos el elemento objetivo.
    const { target } = e;

    // Comprobamos si el elemento clickado es un input de tipo checkbox.
    if (target.matches('input[type="checkbox"]')) {
        // Seleccionamos el li (la tarea) más cercano.
        const taskLi = target.closest('li');

        // Obtenemos el index de la tarea.
        const index = taskLi.getAttribute('data-index');

        // Seleccionamos la tarea correspondiente en el array.
        const currentTask = State.tasks[index];

        // Si la tarea no existe lanzo un error.
        if (!currentTask) throw new Error('La tarea no existe');

        // Invertimos la propiedad "done".
        currentTask.done = !currentTask.done;

        // Actualizamos el localStorage con los cambios.
        localStorage.setItem('tasks', JSON.stringify(State.tasks));

        // Renderizamos la página.
        render();
    }
});

/**
 * #####################################
 * ##  Actualizar la lista de tareas  ##
 * #####################################
 *
 * Crea la función render que se encargará de actualizar las tareas
 * en el HTML cada vez que hagamos un cambio.
 *
 */

function render() {
    // Vaciamos la lista de tareas.
    todoList.textContent = '';

    // Creamos un fragmento de documento.
    const frag = document.createDocumentFragment();

    // Recorremos el array de tareas.
    for (let i = 0; i < State.tasks.length; i++) {
        // Obtenemos mediante destructuring las propiedades de cada tarea.
        const { text, done, date } = State.tasks[i];

        // Creamos un li por tarea.
        const taskLi = document.createElement('li');

        // Creamos un atributo para almacenar la posición de la tarea.
        taskLi.setAttribute('data-index', i);

        // Asignamos el contenido al li.
        taskLi.innerHTML = `
            <input type="checkbox" />
            <p>${text}</p>
            <time>09/11/2022</time>
        `;

        // Si la tarea está marcada como completada "done: true".
        if (done) {
            //  Agregamos la clase "done" al li.
            taskLi.classList.add('done');

            // Además tenemos que agregar el atributo "checked" al checkbox. Lo primero
            // será seleccionar el checkbox.
            const checkbox = taskLi.firstElementChild;
            checkbox.setAttribute('checked', true);
        }

        // Agregamos la tarea como primer hijo del fragmento.
        frag.prepend(taskLi);
    }

    // Al salir del bucle, agregamos el fragmento al ul.
    todoList.append(frag);
}

// Llamamos a la función render cada vez que recargamos la página.
render();
