/**
 * Completa la tabla de temperaturas tomando como referencia
 * este array de ciudades. Tendrás que usar las siguientes
 * clases para cambiar el color de fondo de cada temperatura.
 *
 *  - lower: temp. menor que 4 (fondo azul).
 *
 *  - low: temp. entre 4 y 20 (fondo verde).
 *
 *  - medium: temp. entre 20 y 30 (fondo naranja).
 *
 *  - high: temp. mayor de 30 (fondo rojo).
 *
 */

const cities = [
    {
        name: 'A Coruña',
        min: 17,
        max: 23,
    },
    {
        name: 'Ferrol',
        min: 15,
        max: 32,
    },
    {
        name: 'Lugo',
        min: -20,
        max: 31,
    },
    {
        name: 'Ourense',
        min: 18,
        max: 35,
    },
    {
        name: 'Pontevedra',
        min: 18,
        max: 29,
    },
];

//Importamos función getTemp
//Checkear siempre que la función está bien importada. Llamarla con un console log
import { getTemp } from './helpers.js';

//Seleccionar el elemnto con el que trabajaremos que es el tbody en este caso
const tBody = document.querySelector('tbody');

//Creamos el fragmento de documentos. Para no pushear dentro del loop continuamente
const frag = document.createDocumentFragment();

//Recorrer el array de ciudades
for (const city of cities) {
    //Destructurin con las props del la ciudad
    const { name, max, min } = city;

    //Creamos una fila en la tabla
    const tr = document.createElement('tr');

    //Creamos los 3 td
    const nameTD = document.createElement('td');
    const minTD = document.createElement('td');
    const maxTD = document.createElement('td');

    //Asignamos el valor a los td
    nameTD.textContent = city.name;
    minTD.textContent = city.min;
    maxTD.textContent = city.max;

    //asignamos una clase a los td de las temperaturas
    minTD.classList.add(getTemp(city.min));
    maxTD.classList.add(getTemp(city.max));

    //Agregamos los 3 td como hijos del tr
    tr.append(nameTD, minTD, maxTD);

    //Pusheamos las filas dentro del fragmento
    frag.append(tr);
}

//Pusheamos dentro del tbody el fragmento que contendrá las filas
tBody.append(frag);
