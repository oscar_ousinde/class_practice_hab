/**
 * Completa la tabla de temperaturas tomando como referencia
 * este array de ciudades. Tendrás que usar las siguientes
 * clases para cambiar el color de fondo de cada temperatura (td).
 *
 *  - lower: temp. menor que 4 (fondo azul).
 *
 *  - low: temp. entre 4 y 20 (fondo verde).
 *
 *  - medium: temp. entre 20 y 30 (fondo naranja).
 *
 *  - high: temp. mayor de 30 (fondo rojo).
 *
 */
//El css tiene ya formato y el color depende de la clase que se dea  a cada td
'use strict';

const cities = [
    {
        name: 'A Coruña',
        min: 17,
        max: 23,
    },
    {
        name: 'Ferrol',
        min: 15,
        max: 32,
    },
    {
        name: 'Lugo',
        min: -20,
        max: 31,
    },
    {
        name: 'Ourense',
        min: 18,
        max: 35,
    },
    {
        name: 'Pontevedra',
        min: 18,
        max: 29,
    },
];

//Importamos función getTemp
//Checkear siempre que la función está bien importada. Llamarla con un console log
import { getTemp } from './helpers.js';

//Seleccionar el elemnto con el que trabajaremos que es el tbody en este caso
const tBody = document.querySelector('tbody');

//Creamos el fragmento de documentos. Para no pushear dentro del loop continuamente
const frag = document.createDocumentFragment();

//Recorrer el array de ciudades
for (const city of cities) {
    //Destructurin con las props del la ciudad
    const { name, max, min } = city;

    //Creamos una fila en la tabla
    const tr = document.createElement('tr');

    //Agregamos contenido a la fila (los 3 td). Las clases se pueden cambiar de varias formas. Usando
    //el destructuring o el propio city.min y city.max
    tr.innerHTML = `
        <td>${city.name}</td>
        <td class=${getTemp(city.min)}>${city.min}</td>
        <td class=${getTemp(max)}>${max}</td>
    `;

    //Pusheamos las filas dentro del fragmento
    frag.append(tr);
}

//Pusheamos dentro dell tbody que contendrá las filas
tBody.append(frag);
