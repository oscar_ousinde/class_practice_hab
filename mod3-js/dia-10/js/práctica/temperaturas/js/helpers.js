'use strict';

const getTemp = (temp) => {
    if (temp < 4) {
        return 'lower';
    } else if (temp < 20) {
        return 'low';
    } else if (temp < 30) {
        return 'medium';
    } else {
        return 'high';
    }
};

export { getTemp };
