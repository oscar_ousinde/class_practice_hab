/**
 * #################
 * ## 👻 B O O 👻 ##
 * #################
 *
 * Haz que, cada segundo que pase, el div "boo" aparezca ubicado en una
 * posición aleatoria de la ventana y que el color del body cambie, también
 * de forma aleatoria "rgb(?, ?, ?)".
 *
 * ¡Ojo! No dejes que Boo se salga de los márgenes de la ventana, debemos
 * poder ver a Boo sin hacer scroll en ningún momento.
 *
 * element.getBoundingClientRect() --> devuelve una serie de propiedades
 * del nodo seleccionado, entre ellas, la altura y el ancho del elemento.
 *
 * Bonus point --> haz que aparezcan más emojis acompañando a Boo. Para ello
 *                 deberás crear más divs y seleccionarlos a todos. Esto
 *                 requerirá a mayores que cambies cositas en el código.
 *
 */

'use strict';

//Importamos la función randomValue desde helpers.js
import { randomValue } from './helpers.js';

//Seleccionamos el body. Directamente se puede hacer. Porque el body existe en todos los HTML
const body = document.body;

//Seleccionamos el div. .boo. Hacer un log del const boo para comprobar que está seleccionado correctamente.
//querySelector toma como primer argumento un slector válido de css. (el div en este caso)
//Con el query selectorAll seleccionamos todos los divs con .boo y devuelve un array
const booList = document.querySelectorAll('div.boo');

//Obtenemos el ancho y alto del div boo. Hacemos un destructuring renombramos las variables
//El boo.getBoundingClientRect devuelve un objeto con propeidades que nos interesan en este momento.
//Hacemos por eso destructuring y guardamos las propiedades que queremos.
//obtener el acnho y el alto del primero es suficiente. No cambian de tamaño
//Por esto se calcular ancho y alto del booList[0]. Quitamos trabajo al intervalo
const { height: booHeight, width: booWidth } =
    booList[0].getBoundingClientRect();

//Fragamento de código que se repite cada segundo
setInterval(() => {
    //Recorremos el array de divs:
    for (const boo of booList) {
        //Obtenemos la altura máxima: altura ventana - altura div
        const maxHeight = window.innerHeight - booHeight;

        //Obtenemos la anchura máxima: ancho ventana - ancho div
        const maxWidth = window.innerWidth - booWidth;

        //Modificamos las propeidades top y left del div. Para ello hay que aplicar un position:absolute en el css!!
        //Si damos position absolute en JS el div inicialmente ocupa todo el ancho de la pantalla
        //y no se desplazaría a la derecha con left. A mayores cambiamos el font-size
        boo.style.cssText = `
    top: ${randomValue(maxHeight)}px;
    left: ${randomValue(maxWidth)}px;
    `;
    }

    //Cambiar color body
    body.style.backgroundColor = `rgb(${randomValue(255)},${randomValue(
        255
    )},${randomValue(255)})`;
}, 1000);
