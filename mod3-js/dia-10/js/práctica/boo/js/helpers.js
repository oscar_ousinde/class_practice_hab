'user strict';
//Esto es un módulo para aprender a trabajar con módulos

//Función que retorna un valor aleatorio entre 0 y un valor máximo

const randomValue = (max) => {
    return Math.floor(Math.random() * (max + 1));
};

//Exportamos función
export { randomValue };
