/**
 * Crea un reloj que se actualice en tiempo real:
 *
 *  - La hora debe ir en el <h1>
 *  - La fecha debe ir en el <h2>
 *
 * En función de la hora del día la imagen de fondo debe cambiar.
 * Para este punto puedes ayudarte de las clases:
 *
 *  - morning: a partir de las 7:00.
 *
 *  - afternoon: a partir de las 13:00.
 *
 *  - night: a partir de las 21:00.
 *
 */

'use strict';
//seleccionamos lso elementos con los que vamos a trabajar
const body = document.body;
const h1 = document.querySelector('h1');
const h2 = document.querySelector('h2');

//un set interval apra crear el reloj
setInterval(() => {
    //Obtenemos un objeto con la fecha actual
    const now = new Date();

    //Formateamos la hora
    let formatTime = now.toLocaleTimeString('es-ES');

    //Formateamos la fecha. toLocaleDateString tiene como segundo argumento un objeto para definir el formato
    const formatDate = now.toLocaleDateString('es-ES', {
        day: 'numeric',
        month: 'long',
        year: 'numeric',
    });

    //Obtenemos las horas
    const secs = now.getSeconds();

    //Si los segundos son pares eliminamos los dos puntos
    if (secs % 2 !== 0) {
        formatTime = formatTime.replaceAll(':', ' ');
    }

    //Agregamos la hora al h1
    h1.textContent = formatTime;

    //Agregamos la fechas al h2
    h2.textContent = formatDate;

    //Obtenemos las horas
    const hours = now.getHours();

    //Modificamos la clase del body en fución de la hora (imagen)
    //Nos aseguramos de eliminar la clase anterior antes de adjudicar una nueva
    //Evitamos sumar clases
    if (hours >= 7 && hours < 13) {
        body.classList.remove('night');
        body.classList.add('morning');
    } else if (hours >= 13 && hours < 21) {
        body.classList.remove('morning');
        body.classList.add('afternoon');
    } else {
        body.classList.remove('afternoon');
        body.classList.add('night');
    }
}, 1000);
