'use strict';

//DURANTE LA EJECUCIÓN DE JS

/**
 * Fase de creación
 *
 * 1.Creación del objeto global 'window'.
 *
 * 2. Creación de la palabra reservada 'this'.
 * La palabra this es un pronombre que va a puntar siempre al objeto del contexto actual.
 *
 * Contextos de ejecución en JS. Al llamar a una función se crea un contexto de ejecución.
 * El contexto de ejecución es donde corre JS. El contexto global se crea al iniciar un programa.
 * Al llamar a una función se crea un contexto local para ejecutar esa función y luego desaparece.
 * Al desaparecer el contexto local el control vuelve al contexto global.
 *
 * En el contexto global la palabra this apunta al objeto window. Osea que decir this es lo mismo
 * que decir window. En el contexto global console.log(this === window) es true.
 * Si this está dentro de un método en un objeto y hacemos un console.log nos estamos refiriendo al
 * objeto completo (es su padre). De la misma forma podemos acceder a propeidades del objeto con this.propiedad.
 *
 * 3. Hoisting. JS antes de ejecutar el documento hac euna lectura de variables y funciones en el código.
 * Aquí apunta el nombre de todas las variables y mueve las declaraciones de funnción al inicio del ćódigo
 * de forma interna. Por esto podemos llamar a funciones antes de declararlas. Sin embargo no podemos acceder a ninguna
 * variable antes de llegar a su lectura.
 */

/**
 * Fase de ejecución
 *
 *
 */
