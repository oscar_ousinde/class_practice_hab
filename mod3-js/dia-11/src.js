'use strict';

//Select button
const button = document.querySelector('button');

//AddEvent to click
button.addEventListener('click', () => {
    console.log('Has hecho click!');
});

//EJERCICIO1
//add li to ul al pulsar en button. Cada li con un número distinto
const buttonAddLi = document.querySelector('.add-li');
const items = document.querySelector('ul.items');

let num = 1;

//Creamos el evento al hacer click agregue li
buttonAddLi.addEventListener('click', () => {
    //Creamos el li
    const li = document.createElement('li');

    //Agregamos el contenido al li
    li.innerHTML = `Numero ${num++} <button class="delete">Borrar</button>`;

    //Agregamos el li a ul
    items.append(li);
});

//Agregamos evento click al ul. Aprovechando que los
//Clicks siemrpe burbujean hacia arriba vamos a hacer que
//al pulsar el boton borrar de un li se elimine ese li
items.addEventListener('click', (event) => {
    //Obtenemos la propiedad target(devuelve la etiqueta sobre la que se hace click)del evento
    const { target } = event;
    //Comprobamos si el elemnto sobre el cual hemos hecho click
    //Es exactamente un button con la clase delete
    if (target.matches('button.delete')) {
        //Seleccionamos el li más cercano
        const removeLi = target.closest('li');
        //Eliminamos el li
        removeLi.remove();
    }
});
