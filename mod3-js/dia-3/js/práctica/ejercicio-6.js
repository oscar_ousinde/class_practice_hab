/**
 * #################
 * ## Ejercicio 6 ##
 * #################
 *
 * Crea una función que reciba una altura y dibuje una figura
 * como la que sigue:
 *
 *    1
 *    12
 *    123
 *    1234
 *    12345
 *
 */

'use strict';

let numbers = '';
let height = prompt('Introduce una altura como número entero:');

for (let i = 1; i <= Number(height); i++) {
    numbers += i;
    console.log(numbers);
}
