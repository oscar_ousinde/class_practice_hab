/* #################
 * ## Ejercicio 1 ##
 * #################
 *
 * - Crea un bucle "while" que recorra los números del 0 al 100 saltando de 10 en 10.
 *
 * - Posteriormente crea otro bucle "while" que recorra los números del 100 al 0 de
 *   25 en 25.
 */

'use strict';
let num = 0;

while (num <= 100) {
    console.log(num);
    num = num + 10;
}

while (num <= 100) {
    console.log(num);
    num -= 25;
}
