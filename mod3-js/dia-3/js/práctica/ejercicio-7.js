/**
 * #################
 * ## Ejercicio 7 ##
 * #################
 *
 * Teniendo en cuenta que la clave es "eureka", escribe un algoritmo
 * que os pida una clave.
 *
 * Solo tenemos 3 intentos para acertar:
 *
 *  - Si fallamos los 3 intentos nos mostrará un mensaje indicando
 *    que hemos agotado todos los intentos.
 *
 *  - Si acertamos la clave se nos mostrará un mensaje que indique
 *    que hemos acertado la contraseña.
 *
 */

'use strict';
