/* #################
 * ## Ejercicio 4 ##
 * #################
 *
 * Llegó el momento de poner en práctica todo lo aprendido hasta ahora para...
 * ¡¡DESACTIVAR UNA BOMBA!! Tranquilos, no tenemos que ser expertos en explosivos.
 * Se trata de crear un juego en el que damos al usuario 5 intentos para tratar de
 * desactivar la bomba.
 *
 * Estos son los pasos que debes seguir para tratar de conseguir el objetivo:
 *
 *  - Generar un nº aleatorio del 1 al 10. Existe una función en JavaScript que
 *    nos permite generar un nº al azar, ¿por qué no investigas un poco?
 *
 *  - Una vez hayamos generado el nº de desactivación daremos al usuario un total
 *    de 5 intentos para tratar de averiguar el nº en cuestión.
 *
 *  - Si acierta detenemos el bucle (con un break) y mostramos un mensaje que indica
 *    que la bomba ha sido desactivada. De lo contrario indicamos que la bomba ha explotado.
 */

//Math.ceil(), redondea a la alta
//Math.floor(), redondea a la baja
//una opción es Math.ceil(Math.random() * 10));

'use strict';

/* let randomNum = Math.floor(Math.random() * (11 - 1) + 1);
console.log(`Código de desactivación: ${randomNum}`);
let tryNum = 5;

while (tryNum >= 1) {
    let userPassword = prompt('Introduce la contraseña:');
    if (randomNum != userPassword) {
        tryNum--;
        if (tryNum === 0) {
            console.log('La bomba ha explotado!');
        } else {
            console.log(`Intentos restantes: ${tryNum}`);
        }
    } else {
        console.log('La bomba ha sido desactivada!');
        break;
    }
} */

//Otra forma
//usamos el Number() en el prompt porque este SIEMPRE devuelve strings!
const bombPassword = Math.ceil(Math.random() * 10);
console.log(bombPassword);

let win = false;

for (let intentos = 5; intentos > 0; intentos--) {
    const userPassword = Number(
        prompt(`Intento ${intentos}. Inserte una contraseña:`)
    );
    if (userPassword === bombPassword) {
        win = true;
        break;
    }
}

if (win) {
    alert('Bomba desactivada');
} else {
    alert('BOOM!');
}
