/* #################
 * ## Ejercicio 2 ##
 * #################
 *
 * - Crea un bucle "for" que recorra los números del 0 al 100 saltando de 10 en 10.
 *
 * - Posteriormente crea otro bucle "for" que recorra los números del 100 al 0 de
 *   25 en 25.
 */

'use strict';

for (let count = 0; count <= 100; count += 10) {
    console.log(count);
}

for (let count = 100; count >= 0; count -= 25) {
    console.log(count);
}
