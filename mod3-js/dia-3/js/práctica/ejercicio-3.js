/* #################
 * ## Ejercicio 3 ##
 * #################
 *
 * Escribe un programa que permita al usuario concatenar elementos en un string.
 * El programa finalizará cuando el usuario introduzca el string "fin", y se
 * mostrará por consola el contenido de la variable.
 *
 */

/**
 * PEDIR DATOS POR CONSOLA
 * Tres métodos:
 *
 *      alert('Warning'); --> cuadro de diálogo. Hasta aceptarlo el código
 *      no se ejecuta. Está pausado.
 *
 *      confirm('¿Pregunta?') --> retorna un boolean. Se puede utilziar con bucles
 *      por ejemplo o if para cargar cosas en consola dependiendo de la
 *      respuesta.
 *
 *      prompt('Dime algo') --> pide lo que queramos por consola para introducir valores
 *      por ella. En el prompt los valores van a ser strings. Para operar con números
 *      hay que convertirlos con Number(). Se pueden almacenar esas variables ya
 *      con el método Number(prompt('Show me the numbers!')). Al introducir un valor que
 *      no sea un número tenderemos que usar el isNaN. Aplicar una condicional:
 *
 *          if (isNan(variableA) || isNaN(variableB)) {
 *              throw new Error('Solo se permiten números!')
 *          }
 */

'use strict';

let palabras = '';
let stopWord = '';

/* while (stopWord !== 'fin') {
    palabras += stopWord;
    stopWord = prompt('Introduce una palabra:');
}

if (stopWord === 'fin') {
    console.log(palabras);
} */

//otra forma
while (stopWord !== 'fin') {
    stopWord = prompt('Introduce una palabra:');
    if (stopWord !== 'fin') {
        palabras += `${stopWord}-`;
    } else {
        console.log(palabras);
    }
}
