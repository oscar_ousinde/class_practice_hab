/**
 * #################
 * ## Ejercicio 8 ##
 * #################
 *
 * Escribe un algoritmo que, dado un número entero, visualice por
 * consola si es par o impar. En caso de ser 0 se debe volver a pedir
 * otro número (así hasta que se teclee un número mayor que cero).
 *
 */

'use strict';

let number = '';

while (number <= 0 || isNaN(number)) {
    number = Number(prompt('Dime un número mayor que 0:'));
}

if (number % 2 === 0) {
    alert('El valor es par!');
} else {
    alert('El número es impar!');
}
