'use strict';

/**
 * transformación de num a un Number y arroja NaN.
 * Miramos en isNumberCorrect si el num es NaN o no (true/false)
 * Con el if evaluamos true o false y arrojamos error si es true el NaN
 */
/* const num = Number('123abcd'); //NaN

const isNumberCorrect = isNaN(num);

if (isNumberCorrect) {
    throw new Error('No es número');
} */

//Otra forma del ejemplo anterior más simplifcada
/* const num = Number('123d'); //NaN

if (isNaN(num)) throw new Error('No es número'); */

//ÁMBITO DE ALCANCE DE LAS VARIABLES
/**
 * Desde un ambito global no se puede acceder a una variable
 * en el ámbito local.
 */

/* let age = 15; //ambito global

if (age > 18) {
    let age = 30; //ambito local
    console.log(age);
} else {
    const favNum = 14;
    console.log(age);
}

console.log(age);
console.log(favNum); */ //se pide variable en ambito local desde el global y nos da error.

/**
 * Si cambiamos el age del scope global a 26 la consola arrojaría
 * dos valores de 30 y 26. La variable está declarada dos veces
 * pero en diferentes scopes. No se deben de declarar las variables
 * dos veces. Son malas prácticas. Para JS variables con igual nombre
 * en scopes distintos son distintas!!No se debe de hacer esto.
 * JS busca las variables en los ámbitos locales superiores
 * y finalmente busca en el global (tipo muñecas rusas).
 * El problema de usar var para declarar variables: una variable declarada
 * con var está disponible en todos los scopes. Por seguridad las variables
 * deben estar disponibles en los scopes necesarios solo con let y const.
 */

//BUCLES!
//bucle while
//habitual con valores != numéricos

/* let contador = 0;

while (contador <= 10) {
    console.log(contador);
    contador++;
} */

/**
 * existe también el do while que es diferente. Primero dispara y luego pregunta.
 */

/* let count = 0;

do {
    console.log(count);
    count++;
} while (count <= 10); */

//bucle for
//tiene sentido con valores numéricos solo

for (let count = 0; count <= 10; count++) {
    console.log(count);
}
