/**
 * #################
 * ## Ejercicio 3 ##
 * #################
 *
 * Crea una función que imprima X resultados aleatorios de una
 * quiniela 1 X 2. Ejemplo, si le decimos que imprima 14 resultados:
 *
 *      Resultado 1: 1
 *      Resultado 2: X
 *      Resultado 3: 2
 *      (...)
 *      Resultado 14: 2
 *
 */

'use strict';

//EN ESTE CASO LOS RESULTADOS SALEN AL REVÉS...
/* const quiniela = (resultados) => {
    while (resultados >= 1) {
        const randomNum = Math.ceil(Math.random() * (3 - 0) + 0);
        if (randomNum === 1) {
            console.log(`Resultado ${resultados}: 1`);
        } else if (randomNum === 2) {
            console.log(`Resultado ${resultados}: X`);
        } else if (randomNum === 3) {
            console.log(`Resultado ${resultados}: 2`);
        } else {
            throw new Error('Algo salió mal...');
        }
        resultados--;
    }
};

console.log(quiniela(5)); */

//LA FORMA CORRECTA ES USANDO UN BUCLE FOR!

/* const quiniela = (resultados) => {
    for (let i = 1; i <= resultados; i++) {
        const randomResult = Math.ceil(Math.random() * 3);
        if (randomResult === 1) {
            console.log(`Resultado ${i}: X`);
        } else {
            console.log(`Resultado ${i}: ${randomResult}`);
        }
    }
};

console.log(quiniela(12)); */

//PODREMOS CONSEGUIRLO CON OPERADOR TERNARIO

function quiniela(resultados) {
    for (let i = 1; i <= resultados; i++) {
        const randomNum = Math.ceil(Math.random() * 3);
        randomNum === 3
            ? console.log(`Resultado ${i}: X`)
            : console.log(`Resultado ${i}: ${randomNum}`);
    }
}

console.log(quiniela(5));
