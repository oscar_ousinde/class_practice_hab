/**
 * #################
 * ## Ejercicio 2 ##
 * #################
 *
 * Utiliza lo aprendido sobre funciones para reestructurar el ejercicio de la bomba.
 * Crea la función "deactivateBomb" con los parámetros "limit" y "bombCode":
 *
 *  - limit: nº de intentos que le daremos al usuario.
 *
 */

'use strict';

function deactivateBomb(limit, bombCode) {
    alert(
        `Hay una bomba! Introduce un numero entero del 1 al 10 para desactivarla. Tienes ${limit} intentos totales!`
    );
    const bombPassword = Math.ceil(Math.random() * 10);
    alert(`Código bombPassword: ${bombPassword}`);
    while (limit > 0) {
        bombCode = Number(prompt('Introduce un número (del 1 al 10):'));
        if (bombCode !== bombPassword) {
            limit--;
            alert(`Número de intentos: ${limit}`);
        } else {
            return alert('Bomba desactivada');
        }
    }
    if (limit <= 0) {
        return alert('BOOM!');
    }
}
deactivateBomb(3, 5);
