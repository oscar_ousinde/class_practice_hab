'use strict';

function calculate(a, b, option) {
    if (option === '+') {
        return a + b;
    } else if (option === '-') {
        return a - b;
    } else {
        throw new Error('Tipo de operación incorrecto');
    }
}

// Llamamos a la función y le pasamos los argumentos. Metemos
// el llamado dentro de un console.log para ver lo que retorna.
console.log(calculate(4, 2, '-'));
