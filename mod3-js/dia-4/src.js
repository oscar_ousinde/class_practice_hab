'use strict';

//FUNCIONES
/* function calculate(a, b, option) {
    if (option === '+') {
        return a + b;
    } else if (option === '-') {
        return a - b;
    } else {
        throw new Error('Tipo de operación incorrecto');
    }
} */

//llamar a la función introduciendo los argumentos (se peuden pasar como variable)
//podemos reutilizar las funciones las veces que queramos. Guardamos el valor retornado
//de la función en una variable y nos lo muestra por consola al hacer console.log de esa variable.
/* const result = calculate(4, 2, '+');
console.log(result); */

/**
 * Una función puede no tener parámetros.
 * Si no los tiene hai que poner los (). Se recomienda como máximo
 * de 3 a 4 parámetros.
 * Podemos establecer parámetros con un valor predeterminado a la función antes de
 * pasarle nosotros los argumentos:
 *
 *      function calculate(a, b, option = '+') {}
 *      calculate(4, 3) --> nos da 7 porque aunque no hemos establecido el 3er argumento está predefinido.
 *
 * Se pueden establecer argumentos por defecto pero se tiene en cuenta los argumentos establecidos por
 * el usuario.
 *
 * Normalmente dentro de una función no debería de haber ningún tipo de console.log()
 *
 * Una función debe retornar algo, hai que establecer el valor de retorno sinó devuelve
 * un valor de undefined. En el ejemplo anterior introducimos el return en lugar de los
 * console.log(). El return lleva un break; implícito que detiene la función.
 */

//OTRAS FORMAS DE DECLARAR FUNCIONES

//Declaración de función. Se puede declarar y llamarla antes de la declaración!. Solo con declaración de función.
function sum_A(a, b) {
    return a + b;
}

console.log(sum_A(4, 5));

//Expresión de función.
//No se puede acceder a la variable sum_B antes de declarar la expresión de función.
//No se suele utilziar salvo en los objetos. Crear funciones dentro de objetos.
const sum_B = function (a, b) {
    return a + b;
};

//Función flecha
//Si el código de la función es una sola línea podemos omitir las llaves y el return es implícito.
const sum_C = (a, b) => {
    return a + b;
};

const sum_D = (a, b) => a + b;

console.log(sum_C(4, 5));
console.log(sum_D(9, 5));

//Llama mal a la función sum aporta por consola el esqueleto de la misma
console.log(sum_B);
