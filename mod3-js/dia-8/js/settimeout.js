/**
 * ################
 * ## setTimeout ##
 * ################
 *
 * Permite posponer la ejecución de un fragmento de código. Este método
 * recibe dos argumentos:
 *
 *  - Una función que se ejecutará pasado un determinado tiempo.
 *
 *  - Un valor numérico en milisegundos que será el tiempo que tardará la
 *    función anterior en ejecutarse.
 *
 */

//Este método existe solo en el objeto global window!!
'use strict';

const myTimeout = setTimeout(() => {
    console.log('Hola Mundo!');
}, 3000);

//Deshabilitamos un setTimeout guardado como referencia en una variable
//Igual nos interesa desactivarlo en un determinado momento o dando una condición
// clearTimeout(myTimeout);
