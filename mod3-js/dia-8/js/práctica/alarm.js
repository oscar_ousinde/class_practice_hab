'use strict';
//función que recibe como argumento un número de segundos
//Cuando llega a cero imprime en consola --> ALARMA
function alarm(seconds) {
    //Guardamos el intervalo para poder cancelarlo más adelante
    const myInterval = setInterval(() => {
        //Si los segundos son cero imprimimos sinó descontamos en el contador
        if (seconds === 0) {
            clearInterval(myInterval);
            console.log('ALARMAAAA!');
        } else {
            console.log(seconds--);
        }
    }, 1000);
}

alarm(10);
