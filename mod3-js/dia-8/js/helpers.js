'use strict';

const PI = 3.1416;
const sum = (a, b) => a + b;
const sub = (a, b) => a - b;

export { sum, sub, PI };
