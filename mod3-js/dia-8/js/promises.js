/**
 * ##############
 * ## Promises ##
 * ##############
 *
 * Una promesa es un valor que, en el momento en que lo solicitamos, no sabemos
 * si lo vamos a obtener o no.
 *
 * Este símil podría servir para explicarlo mejor: tu madre te ha prometido que
 * si apruebas todo la semana que viene te regalará un teléfono. A día de hoy
 * tú no sabes si tendrás ese teléfono o no. La semana que viene en función de
 * si apruebas o suspendes tendrás o no el teléfono.
 *
 * Decimos entonces que las promesas tienen tres estados posibles:
 *
 *  - pending: tu madre te ha prometido un teléfono. Ahora mismo no sabes si
 *             te lo regalará o no.
 *
 *  - fulfilled: has aprobado y finalmente logras que te regalen el teléfono.
 *
 *  - rejected: has suspendido así que olvídate del teléfono.
 *
 * La Web API "fetch" nos permite hacer peticiones a un servidor y recibir una
 * respuesta. El tiempo que tarda un servidor en devolver una respuesta es algo
 * que no podemos preveer, pero no es inmediato.
 *
 * Fetch retorna siempre una promesa. Es imposible visualizar el contenido de una
 * promesa utilizando código síncrono dado que el valor de una promesa no es algo
 * inmediato, asíque tenemos que esperar a que se resuelva de algún modo.
 *
 * Tenemos dos opciones:
 *
 *  - Utilizar los métodos "then" y "catch" presentes en todos los objetos de tipo
 *    Promise
 *
 *  - Utilizar una función asíncrona: async / await.
 *
 */

'use strict';

//fetch envia una petición al servidor de la url
//No vemos nada y si hacemos un log de este fecth nos dará una Promesa pendiente.
//El fetch nos da una promesa y para acceder a ella hay que hacerlo con código asíncrono
//El método then nos da el valor de una promesa fullfilled. Si la promesa es rejected será capturada por catch. Ambos reciben un callback
//Damos un callback a cada método en este caso con parámetros response y err
//La promesa al entrar en .then nos devuelve un objeto llamado Response. Es la respuesta del servidor al cliente
//Este objeto tiene en su propiedad body la info requerida en un formato de buffer de datos.
//Tanto then como catch son asíncronos. Hasta que la promesa no tenga su estado fullfilled o rejected no se ejecutan. Son asíncronos!!
//response.json() busca el body del objeto Response y lo transforma en codigo JS. Pero esto devuelve otra promesa. Agregamos de nuevo otro then! y listo
fetch('https://rickandmortyapi.com/api')
    .then((response) => response.json())
    .then((body) => console.log(body))
    .catch((err) => console.error(err));
