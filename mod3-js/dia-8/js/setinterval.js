'use strict';

/**
 * #################
 * ## setInterval ##
 * #################
 *
 * Permite repetir una y otra vez un fragmento de código. Este método recibe
 * dos argumentos:
 *
 *  - Una función que se ejecutará cada cierto tiempo.
 *
 *  - Un valor numérico en milisegundos que será el tiempo que transcurrirá
 *    entre las distintas repeticiones de la función anterior.
 *
 */

//Para hacer por ejemplpo un reloj. Carga algo cada x segundos

let seconds = 1;
const myInterval = setInterval(() => {
    console.log(seconds++);
}, 1000);

//Deshabilitamos el setInterval con un setTimeout

setTimeout(() => {
    clearInterval(myInterval);
}, 10000);
