'use strict';
/* fetch('https://rickandmortyapi.com/api')
    .then((response) => response.json())
    .then((body) => console.log(body))
    .catch((err) => console.error(err)); */

//Utilizamos una función asíncrona para acceder al API
const getData = async () => {
    //El bloque try sirve para poder manipular los errores en caso de que sucedan
    try {
        //Con el await esperamos a que la promesa se resuelva para continuar
        const response = await fetch(
            //Accedemos con el query params a la página que queramos de las disponibles ?page4
            //Con esto podemos filtrar (ver la documentación) por ejemplo ?status=dead&species=alien
            'https://rickandmortyapi.com/api/character?status=dead&species=alien'
        );

        //Queremos la propiedad body del objeto response. Usamos el json y retorna una promesa
        const body = await response.json();

        console.log(body);
    } catch (err) {
        //podríamos manipular el error en caso de que algo haya salido mal en el bloque try
        console.error(err.message);
    }
};

getData();
