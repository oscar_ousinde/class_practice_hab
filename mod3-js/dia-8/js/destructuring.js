'use strict';

//DESTRUCTURING CON ARRAYS
const colors = ['amarillo', 'azul', 'lila'];

//Crea las variables const a, const b y const c a partir del array colors
//Podemos saltarnos una posición dejando el indice de elemento vacío entre comillas

const [a, b, c] = colors;
const [A, , C] = colors;

console.log(a);
console.log(b);
console.log(c);
console.log(A);
console.log(C);

//DESTRUCTURING CON OBJETOS
const Car = {
    brand: 'Opel',
    model: 'Corsa',
    color: 'pink',
    doors: 5,
};

//Importante respetar el nombre de la propiedad!!
const { brand, model, ...others } = Car;

console.log(brand, model);
console.log(others);
