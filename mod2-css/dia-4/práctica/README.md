# Ejercicio

Tomando como referencia las imagenes de ejemplo realiza un diseño responsive ayudándote de las media queries y flexbox. Puedes realizar las modificaciones que consideres oportunas en el fichero `index.html`

En la versión para móvil verás un menú desplegable. ¿Cómo puedo hacer un menú desplegable? Google está lleno de respuestas, ¿te animas a encontrarlas? [Haz click aquí](https://www.youtube.com/watch?v=TeUF-MQZfAw) si necesitas ayuda. ;)

¿Te parece muy complicado? No te preocupes, puedes omitir este menú desplegable y optar por una alternativa distinta.

## Diseño móvil

![Ejemplo móvil](./example/mobile.png)

## Menú desplegable (solo en móvil)

![Menu desplegable](./example/menu.png)

## Diseño tablet (a partir de 700px)

![Ejemplo tablet](./example/tablet.png)

## Diseño ordenador ( a partir de 1200px)

![Ejemplo ordenador](./example/desktop.png)

