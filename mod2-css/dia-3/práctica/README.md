# Ejercicio

Haz las modificaciones que consideres necesarias en el ejercicio anterior para dar al documento `index.html` un aspecto similar a la siguiente imagen:

![Ejemplo página](./ejemplo.png)

P.D: en este caso sí se permite modificar el html (lo que no quiere decir que sea algo necesario para la resolución del ejercicio).

