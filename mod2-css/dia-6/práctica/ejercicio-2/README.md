# Ejercicio

Crea un diseño responsive que se adapte a dispositivos móviles, tablets (a partir de 700px) y a ordenadores (a partir de 1200px) utilizando exclusivamente grid.

-   No modifiques el `index.html`.

-   No hay foto para el diseño móvil. Todos los elementos van en bloque (unos encima de otros) por lo que no deberías tener problema.

## Diseño de mobil

![Diseño de mobil 1](./pics/mobile1.png)
![Diseño de mobil 2](./pics/mobile2.png)
![Diseño de mobil 3](./pics/mobile3.png)

## Diseño de tablet

![Diseño de tablet](./pics/tablet.png)

## Diseño de ordenador

![Diseño de tablet](./pics/desktop.png)
